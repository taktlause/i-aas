// Require the necessary discord.js classes
const morse = require("morse");
const fs = require("node:fs");
const path = require("node:path");
const { Client, Collection, Events, GatewayIntentBits } = require("discord.js");
const { token } = require("./config.json");

// Create a new client instance
const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
  ],
});

client.commands = new Collection();

const commandsPath = path.join(__dirname, "commands");
const commandFiles = fs
  .readdirSync(commandsPath)
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const filePath = path.join(commandsPath, file);
  const command = require(filePath);
  // Set a new item in the Collection with the key as the command name and the value as the exported module
  if ("data" in command && "execute" in command) {
    client.commands.set(command.data.name, command);
  } else {
    console.log(
      `[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`
    );
  }
}

// When the client is ready, run this code (only once)
// We use 'c' for the event parameter to keep it separate from the already defined 'client'
client.once(Events.ClientReady, (c) => {
  c.user.setPresence({ activities: [{ name: "I ÅS!" }], status: "invisible" });
  console.log(`Ready! Logged in as ${c.user.tag}`);
});

// Log in to Discord with your client's token
client.login(token);

const regex = /[I(Ås) \/]{7,}/gi;
client.on(Events.MessageCreate, async (message) => {
  if (message.content.match(regex)) {
    message.reply(
      message.content.replaceAll(regex, (match) =>
        morse
          .decode(
            match
              .toLowerCase()
              .replaceAll("ås", "-")
              .replaceAll("i", ".")
              .replaceAll(";", "-.-.-")
              .replaceAll("/", ".......")
          )
          .replaceAll("Á", "Å")
          .replaceAll("Ö", "Ø")
          .replaceAll("Ä", "Æ")
      )
    );
  }
});
